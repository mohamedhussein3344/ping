export class NotificationMessage {
  constructor(
    id: string,
    header: string,
    body: string,
    messageType: string,
    seen: boolean
  ) {}
}
