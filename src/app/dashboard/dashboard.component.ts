import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { PingService } from "ng2-testable-lib";
import { NotificationMessage } from "ng2-testable-lib";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  showError: boolean = false;
  errorMessage: string;

  notificationMessage: NotificationMessage = <NotificationMessage>{};

  constructor(
    private notificationsService: PingService
  ) { }

  ngOnInit() {

  }

  notifyNow() {

    if(this.notificationMessage['header'] == undefined) {
      this.showError = true;
      this.errorMessage = 'Please insert header'
    }
    else if(this.notificationMessage['body'] == undefined) {
      this.showError = true;
      this.errorMessage = 'please insert notification body';
    }else if (this.notificationMessage['messageType'] == undefined) {
      this.showError = true;
      this.errorMessage = 'please choose type of notification';
    }else {
      this.showError = false;

      /*
        localStorage.setItem('notificationArrStorage' , JSON.stringify(this.notificationArr));
        const localStorageData = localStorage.getItem('notificationArrStorage');
        if (localStorageData) {
          this.notificationArr = JSON.parse(localStorageData);
        } else {
          this.notificationArr = [];
        }
      */

      let message = _.cloneDeep(this.notificationMessage);
      this.notificationsService.addNotification(message);

    }

  }
}
