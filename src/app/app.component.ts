import { Component } from '@angular/core';
import {NotificationMessage} from './dashboard/notification-message'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  notificationArr: NotificationMessage[];
  showNow = false;
  notifyNowDashboard(showNotifications: boolean) {
    if (showNotifications) {
      const localStorageData = localStorage.getItem('notificationArrStorage');
      if (localStorageData) {
        this.notificationArr = JSON.parse(localStorageData);
        console.log('notification', this.notificationArr);
      }
      this.showNow = true;
    }
  }
}


